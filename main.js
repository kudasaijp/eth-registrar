async function check(name) {
  const data = Web3.utils.padRight('0x02571be3' + Web3.utils.stripHexPrefix(Web3.utils.utf8ToHex(name)), 20);
  const res = await fetch('https://rpc.ankr.com/eth', {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      id: 43,
      jsonrpc: '2.0',
      method: 'eth_call',
      params: [
        {
          data,
          to: '0x5564886ca2c518d1964e5fcea4f423b41db9f561',
        },
        'latest',
      ],
    }),
  });

  const json = await res.json();
  return { name, available: Web3.utils.hexToNumberString(json.result) === '0' };
}

document.querySelector('form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const submitButton = document.getElementById('submit-button');
  submitButton.disabled = true;
  submitButton.value = 'Checking...';

  const names = e.target.elements.names.value.trim().split(/\r\n|\n/);
  const results = await Promise.all(names.map(check));

  let html = '';
  for (const result of results) {
    const status = result.available ? '○' : '×';
    const className = result.available ? 'available' : 'reserved';
    html += `<div class="${className}">${result.name} ${status}</div>`;
  }

  submitButton.disabled = false;
  submitButton.value = 'Check';

  const output = document.getElementById('output');
  output.innerHTML = html + output.innerHTML;
});

document.getElementById('hide-reserved').addEventListener('change', (e) => {
  const output = document.getElementById('output');
  output.classList.toggle('hide-reserved', e.target.checked);
});
